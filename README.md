# README #

1. This project using Spring Framework to create and test.
2. The main implmentation is using polymorphism which is a comman way where there have multiple "form submitted"
3. All the test scenario and output can be find Solution.java insided com.coding.singtel.core packaged

# REST API Design for Animals

1. We need to create a java class with @RestContoller and @RequestMapping annotation
2. define and expose the endpoint e.g. "/animal/{value_animal}"
3. Define a return DTO object like below
	
	public AnimalDto {
		private String animalName;
		private String animalType;
		private String fly;
		private String swim;
		private String sing;
		private String walk;
	}
4. if for query animal, we can using @GettingMapping (HTTP Get Method) like blow:

	@Getmapping("/animal/{animal}")
	public ResponeEntity<AnimalDTO> getAnimal(String animal){
		
		....
		// Validation or Business logic can implement here;
		....
		return ResponseEntity.status(HttpStatus.OK).body(new AnimalDTO());
	}