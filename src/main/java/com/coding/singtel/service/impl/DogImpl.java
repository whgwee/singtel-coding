package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

@Service
public class DogImpl extends BirdImpl {

	@Override
	public boolean sing() {
		System.out.println("Woof, woof");
		return true;
	}

}
