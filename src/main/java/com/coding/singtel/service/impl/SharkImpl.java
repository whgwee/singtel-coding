package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

import com.coding.singtel.service.Fish;

@Service
public class SharkImpl implements Fish {

	@Override
	public boolean fly() {
		System.out.println("I cannot fly");
		return false;
	}

	@Override
	public boolean walk() {
		System.out.println("I don't walk");
		return true;
	}

	@Override
	public boolean sing() {
		System.out.println("I don't sing");
		return true;
	}

	@Override
	public boolean swim() {
		System.out.println("I can swim");
		return true;
	}

	@Override
	public boolean color() {
		System.out.println("I'm grey color");
		return true;
	}

	@Override
	public boolean size() {
		System.out.println("I'm large");
		return true;
	}

	@Override
	public boolean eat() {
		System.out.println("I eat a lot of fish");
		return true;
	}

	@Override
	public boolean makeJokes() {
		return false;
	}
}
