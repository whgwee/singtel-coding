package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

@Service
public class DuckImpl extends BirdImpl {

	@Override
	public boolean sing() {
		System.out.println("Quack, quack");
		return true;
	}

	@Override
	public boolean swim() {
		System.out.println("I can swim");
		return true;
	}

}
