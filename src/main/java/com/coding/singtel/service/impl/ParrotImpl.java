package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

import com.coding.singtel.service.Animal;

@Service
public class ParrotImpl extends BirdImpl {

	Animal animal = null;

	@Override
	public boolean sing() {

		if (animal == null) {
			animal = new DuckImpl();
		}

		animal.sing();
		return true;
	}

	public void livingWithAnimal(Animal animal) {
		this.animal = animal;
	}

}
