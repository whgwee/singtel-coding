package com.coding.singtel.service.impl;

import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class RoosterImpl extends ChickenImpl {

	private String language = null;

	@Override
	public boolean sing() {

		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		map.put("Danish", "kykyliky");
		map.put("Dutch", "kukeleku");
		map.put("Finnish", "kukko kiekuu");
		map.put("French", "cocorico");
		map.put("German", "kikeriki");
		map.put("Greek", "kikiriki");
		map.put("Hebrew", "coo-koo - ri-koo");
		map.put("Hungarian", "kukuriku");
		map.put("Italian", "chicchirichi");
		map.put("Japanese", "ko - ke - kok - ko -o");
		map.put("Portuguese", "cucurucu");
		map.put("Russian", "kukareku");
		map.put("Swedish", "kuckeliku");
		map.put("Turkish", "kuk-kurri-kuuu");
		map.put("Urdu", "kuklooku");

		if (StringUtils.isNoneBlank(language) && StringUtils.isNotBlank(map.get(language)))
			System.out.println(map.get(language));
		else
			System.out.println("Cock-a-doodle-doo");
		return true;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String language() {
		return this.language;
	}
}
