package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

import com.coding.singtel.service.Fish;

@Service
public class ClownfishImpl implements Fish {

	@Override
	public boolean fly() {
		System.out.println("I cannot fly");
		return false;
	}

	@Override
	public boolean walk() {
		System.out.println("I don't walk");
		return true;
	}

	@Override
	public boolean sing() {
		System.out.println("I don't sing");
		return true;
	}

	@Override
	public boolean swim() {
		System.out.println("I can swim");
		return true;
	}

	@Override
	public boolean color() {
		System.out.println("I'm orange color");
		return true;
	}

	@Override
	public boolean size() {
		System.out.println("I'm small");
		return true;
	}

	@Override
	public boolean eat() {
		return false;
	}

	@Override
	public boolean makeJokes() {
		System.out.println("I make jokes");
		return true;
	}

}
