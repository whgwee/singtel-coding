package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

@Service
public class ChickenImpl extends BirdImpl {

	@Override
	public boolean fly() {
		System.out.println("I cannot fly");
		return true;
	}

	@Override
	public boolean sing() {
		System.out.println("Cluck, cluck");
		return true;
	}
}
