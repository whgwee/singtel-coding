package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

import com.coding.singtel.service.Animal;
import com.coding.singtel.service.Fish;

@Service
public class DolphinImpl implements Animal {

	Fish fish;

	public void isConsiderFish(Fish fish) {
		this.fish = fish;
	}

	@Override
	public boolean fly() {
		this.fish.fly();
		return this.fish.fly();
	}

	@Override
	public boolean walk() {
		this.fish.walk();
		return false;
	}

	@Override
	public boolean sing() {
		this.fish.sing();
		return false;
	}

	@Override
	public boolean swim() {
		this.fish.swim();
		return true;
	}

}
