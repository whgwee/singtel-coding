package com.coding.singtel.service.impl;

import org.springframework.stereotype.Service;

import com.coding.singtel.service.Animal;

@Service
public class ButterflyImpl implements Animal {

	boolean isCaterpillor = false;

	@Override
	public boolean fly() {
		if (isCaterpillor) {
			System.out.println("I cannot fly");
			return false;
		} else {
			System.out.println("I can fly");
			return true;
		}

	}

	@Override
	public boolean walk() {
		if (isCaterpillor) {
			System.out.println("I can walk(crawl)");
			return true;
		} else {
			System.out.println("I can walk");
			return false;
		}
	}

	@Override
	public boolean sing() {
		System.out.println("I cannot sing");
		return true;
	}

	@Override
	public boolean swim() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isCaterpillor() {
		return this.isCaterpillor;
	}

	public void setCaterpillor(boolean isCaterpillor) {
		this.isCaterpillor = isCaterpillor;
	}
}
