package com.coding.singtel.service;

public interface Animal {

	public boolean fly();

	public boolean walk();

	public boolean sing();

	public boolean swim();
}
