package com.coding.singtel.service;

public interface Fish extends Animal {

	public boolean color();

	public boolean size();

	public boolean eat();

	public boolean makeJokes();
}
