package com.coding.singtel.core;

import java.util.Arrays;
import java.util.List;

import com.coding.singtel.service.Animal;
import com.coding.singtel.service.impl.BirdImpl;
import com.coding.singtel.service.impl.ButterflyImpl;
import com.coding.singtel.service.impl.CatImpl;
import com.coding.singtel.service.impl.ChickenImpl;
import com.coding.singtel.service.impl.ClownfishImpl;
import com.coding.singtel.service.impl.DogImpl;
import com.coding.singtel.service.impl.DolphinImpl;
import com.coding.singtel.service.impl.DuckImpl;
import com.coding.singtel.service.impl.ParrotImpl;
import com.coding.singtel.service.impl.RoosterImpl;
import com.coding.singtel.service.impl.SharkImpl;

public class Solution {

	public static void main(String[] args) {

		BirdImpl bird = new BirdImpl();
		System.out.println("===========Bird===========");
		bird.fly();
		bird.sing();
		bird.walk();

		DuckImpl duck = new DuckImpl();
		System.out.println("===========Duck===========");
		duck.fly();
		duck.sing();
		duck.walk();
		duck.swim();

		ChickenImpl chicken = new ChickenImpl();
		System.out.println("===========Chicken===========");
		chicken.fly();
		chicken.sing();

		CatImpl cat = new CatImpl();
		System.out.println("===========Cat===========");
		cat.fly();
		cat.sing();
		cat.walk();
		cat.sing();

		DogImpl dog = new DogImpl();
		System.out.println("===========Dog===========");
		dog.fly();
		dog.sing();
		dog.walk();
		dog.sing();

		RoosterImpl rooster = new RoosterImpl();
		System.out.println("===========Rooster===========");
		rooster.sing();

		DolphinImpl dolphin = new DolphinImpl();
		System.out.println("===========Dolphin===========");
		dolphin.isConsiderFish(new SharkImpl());
		dolphin.fly();
		dolphin.sing();
		dolphin.walk();
		dolphin.sing();

		// =================================

		Animal[] animals = new Animal[] { new BirdImpl(), new ButterflyImpl(), new CatImpl(), new ChickenImpl(),
				new ClownfishImpl(), new DogImpl(), new DuckImpl(), new ParrotImpl(), new RoosterImpl(),
				new SharkImpl() };

		Integer canWalk = 0;
		Integer canFly = 0;
		Integer canSing = 0;
		Integer canSwim = 0;

		for (Animal animal : animals) {
			if (animal.walk())
				canWalk++;
			if (animal.fly())
				canFly++;
			if (animal.sing())
				canSing++;
			if (animal.swim())
				canSwim++;
		}

		System.out.println("Animals Can Walk : " + canWalk);
		System.out.println("Animals Can Fly : " + canFly);
		System.out.println("Animals Can Sing : " + canSing);
		System.out.println("Animals Can Swim : " + canSwim);

		List<String> languageList = Arrays.asList("Danish", "Dutch", "Finnish", "French", "German", "Greek", "Hebrew",
				"Hungarian", "Italian", "Japanese", "Portuguese", "Russian", "Swedish", "Turkish", "Urdu");

		RoosterImpl roosterWithLanguage = new RoosterImpl();

		for (String language : languageList) {
			roosterWithLanguage.setLanguage(language);
			roosterWithLanguage.sing();
		}

	}
}
