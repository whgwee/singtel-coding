package com.coding.singtel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SingtelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SingtelApplication.class, args);
	}

}
